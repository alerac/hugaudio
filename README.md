# Hugaudio

Hugaudio is a hugo theme for a project which aims at creating a website to host live streams and their recordings.

## Installation

Create a new [hugo](https://gohugo.io/getting-started/quick-start/) site and in command line type the following commands

```bash
cd MyNewHugoSite
git init
git submodule add https://framagit.org/alerac/hugaudio.git
```

## Usage

More documentation about using [hugo](https://gohugo.io/).

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## Changelog

The changelog is to be found in the `CHANGELOG.md` file.

To install `auto-changelog`:

```bash
yarn add auto-changelog
```

The configuration is in the `.auto-changelog` file.

To generate or update the changelog:

```bash
yarn auto-changelog
```

## Authors

[Alexandre Racine](https://alex-racine.ch)  
[iGor milhit](https://igor.milhit.ch/)

## License
[MIT](https://choosealicense.com/licenses/mit/)
